import {
  VStack,
  Heading,
  Box,
  StackProps,
  useMultiStyleConfig,
} from '@chakra-ui/react'
import * as React from "react";
import {ReactTyped} from "react-typed";

export interface SectionTitleProps extends Omit<StackProps, 'title'> {
  title: React.ReactNode
  description?: React.ReactNode
  align?: 'left' | 'center'
  variant?: string
}

export const SectionTitleTyped: React.FC<SectionTitleProps> = (props) => {
  const { title, description, align, variant, ...rest } = props
  const styles = useMultiStyleConfig('SectionTitle', { variant })

  // @ts-ignore
  return (
    <VStack
      sx={styles.wrapper}
      alignItems={align === 'left' ? 'flex-start' : 'center'}
      spacing={4}
      {...rest}
    >
      <Heading sx={styles.title} as="h4">
        {title}
      </Heading>
      {description && (
          <>
          <Box sx={styles.description} textAlign={align}>
            <ReactTyped strings={
              description
            }
                        typeSpeed={80}
                        backSpeed={50}
                        loop={true} />
        </Box>
          </>

      )}
    </VStack>
  )
}
