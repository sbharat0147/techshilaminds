import { chakra, SimpleGrid } from '@chakra-ui/react'
import { Section, SectionProps, SectionTitle } from 'components/section'
import {useAnimation} from "framer-motion";
import {useEffect} from "react";
import {Em} from "components/typography";

interface FaqProps extends Omit<SectionProps, 'title' | 'children'> {
  title?: React.ReactNode
  description?: React.ReactNode
  items: { q: React.ReactNode; a: React.ReactNode }[]
}

export const AboutUs: React.FC<FaqProps> = (props) => {
  const {
    title = 'Frequently asked questions',
    description,
    items = [],
  } = props

    const controls = useAnimation();

    useEffect(() => {
        controls.start({
            opacity: 1,
            y: 0,
            transition: {
                duration: 0.5,
                ease: "easeOut",
            },
        });
    }, [controls]);
  return (
    <Section id="faq">
      <SectionTitle title={title} description={description} />

      <SimpleGrid columns={[1, null, 1]} spacingY={10} spacingX="20">
        {items?.map(({ q, a }, i) => {
          return <FaqItem key={i} question={''} answer={a}  controls={controls}/>
        })}

      </SimpleGrid>
    </Section>
  )
}

export interface FaqItemProps {
  question: React.ReactNode
  answer: React.ReactNode
    controls: ReturnType<typeof useAnimation>;
}

const FaqItem: React.FC<FaqItemProps> = ({ question, answer,controls }) => {
  return (
      <chakra.dl>
          <chakra.dt fontWeight="semibold"
                     mb="2"
                     _hover={{ color: 'blue.500', transition: 'color 0.3s' }}>
              {question}
          </chakra.dt>
          <chakra.dd color="muted"
                     _hover={{ color: 'blue.500', transition: 'color 0.3s' }}>{answer}</chakra.dd>
      </chakra.dl>
  )
}
