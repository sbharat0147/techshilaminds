import * as React from "react";
import type { NextPage } from "next";
import Image from "next/image";
import {
  Container,
  Box,
  Stack,
  HStack,
  ButtonGroup,
  Button,
  Icon,
  Heading,
  Text,
  Wrap,
  Tag,
  useClipboard,
  IconButton,
  VStack,
  Flex,
} from "@chakra-ui/react";
import { SEO } from "components/seo/seo";

import { FallInPlace } from "components/motion/fall-in-place";
import { Hero } from "components/hero";
import { Link, Br } from "@saas-ui/react";
import { Em } from "components/typography";
import { NextjsLogo, ChakraLogo } from "components/logos";
import {
  FiArrowRight,
  FiBox,
  FiCheck,
  FiCode,
  FiCopy,
  FiFlag,
  FiGrid,
  FiLock,
  FiSearch,
  FiSliders,
  FiSmile, FiStar,
  FiTerminal,
  FiThumbsUp,
  FiToggleLeft,
  FiTrendingUp, FiUser,
  FiUserPlus,
} from "react-icons/fi";
import { Features } from "components/features";
import { BackgroundGradient } from "components/gradients/background-gradient";
import { Faq } from "components/faq";
import { Pricing } from "components/pricing/pricing";

import { ButtonLink } from "components/button-link/button-link";
import { Testimonial, Testimonials } from "components/testimonials";

import faq from "data/faq";
import testimonials from "data/testimonials";
import pricing from "data/pricing";

import {
  Highlights,
  HighlightsItem,
  HighlightsTestimonialItem,
} from "components/highlights";
import {ReactTyped} from "react-typed";
import {AboutUs} from "components/aboutUs";

const Home: NextPage = () => {
  return (
    <Box>
      <SEO
          title="TechShila Minds"
        description="Train, Build & Scale with US"
      />
      <Box>
        <HeroSection />
        <PricingSection />

        <HighlightsSection />

        {/*<FeaturesSection />*/}

        <TestimonialsSection />



        <FaqSection />
      </Box>
    </Box>
  );
};

const HeroSection: React.FC = () => {
  return (
    <Box position="relative" overflow="hidden">
      <BackgroundGradient height="100%" zIndex="-1" />
      <Container maxW="container.xl" pt={{ base: 40, lg: 60 }} pb="40">
        <Stack direction={{ base: "column", lg: "row" }} alignItems="center">
          <Hero
            id="home"
            justifyContent="flex-start"
            px="0"
            title={
              <FallInPlace>
                <ReactTyped strings={[
                  'Train',
                  'Build',
                  'Scale',
                ]}
                            typeSpeed={80}
                            backSpeed={50}
                            loop={true} />
                <Br /> with us
              </FallInPlace>
            }
            description={
              <FallInPlace delay={0.4} fontWeight="medium">
                We are a <Em>Team Of   <ReactTyped strings={[
                    'Professionals',
                    'Trainers',
                    'Developers',
                    'Consultants',
                    'Experts'
              ]}
                                                   typeSpeed={80}
                                                   backSpeed={50}
                                                   loop={true} /></Em>
                <Br /> that doesn&apos;t get in your way and helps you <Br />{" "}
                to train, build and scale your IT Products.
              </FallInPlace>
            }
          >
            <FallInPlace delay={0.8}>
              {/*<HStack pt="4" pb="12" spacing="8">*/}
              {/*  <NextjsLogo height="28px" /> <ChakraLogo height="20px" />*/}
              {/*</HStack>*/}

              <ButtonGroup spacing={4} alignItems="center">
                {/*<ButtonLink colorScheme="primary" size="lg" href="/signup">*/}
                {/*  Sign Up*/}
                {/*</ButtonLink>*/}
                {/*<ButtonLink*/}
                {/*  size="lg"*/}
                {/*  href="https://demo.saas-ui.dev"*/}
                {/*  variant="outline"*/}
                {/*  rightIcon={*/}
                {/*    <Icon*/}
                {/*      as={FiArrowRight}*/}
                {/*      sx={{*/}
                {/*        transitionProperty: "common",*/}
                {/*        transitionDuration: "normal",*/}
                {/*        ".chakra-button:hover &": {*/}
                {/*          transform: "translate(5px)",*/}
                {/*        },*/}
                {/*      }}*/}
                {/*    />*/}
                {/*  }*/}
                {/*>*/}
                {/*  View demo*/}
                {/*</ButtonLink>*/}
              </ButtonGroup>
            </FallInPlace>
          </Hero>
          <Box
            height="600px"
            position="absolute"
            display={{ base: "none", lg: "block" }}
            left={{ lg: "60%", xl: "55%" }}
            width="80vw"
            maxW="1100px"
            margin="0 auto"
          >
            <FallInPlace delay={1}>
              <Box overflow="hidden" height="100%">
                <Image
                  src="/static/screenshots/test2.png"
                  layout="fixed"
                  width={1200}
                  height={762}
                  alt="Screenshot of a ListPage in Saas UI Pro"
                  quality="75"
                  priority
                />
              </Box>
            </FallInPlace>
          </Box>
        </Stack>
      </Container>

      <Features
        id="benefits"
        columns={[1, 2, 4]}
        iconSize={10}
        innerWidth="container.xl"
        pt="20"
        features={[
          {
            title: "+40",
            icon: FiSmile,
            number:'+40',
            description: "Active Clients",
            iconPosition: "left",
            delay: 0.6,
          },
          {
            title: "+400",
            number:'+400',
            icon: FiSliders,
            description:
              "Delivered Projects",
            iconPosition: "left",
            delay: 0.8,
          },
          {
            title: "+10",
            number:'+10',
            icon: FiStar,
            description:
              "Winning Awards",
            iconPosition: "left",
            delay: 1,
          },
          {
            title: "+50",
            icon: FiUser,
            number:'+50',
            description:
              "Our Team Members",
            iconPosition: "left",
            delay: 1.1,
          },
        ]}
        reveal={FallInPlace}
      />
    </Box>
  );
};

const HighlightsSection = () => {
  const { value, onCopy, hasCopied } = useClipboard("yarn add @saas-ui/react");

  return (
    <Highlights>
      <HighlightsItem colSpan={[1, null, 2]} title="Trainings">
        <VStack alignItems="flex-start" spacing="8">
          <Text color="muted" fontSize="xl">
            At <Em>TechShila Minds</Em>,
            we are a dedicated team of
            seasoned professionals committed to
            delivering comprehensive training across all IT domains.
            Our mission is to empower individuals and organizations by
            providing top-notch training that drives success in the
            ever-evolving tech landscape. Become part of a vibrant community
            of learners and professionals who are passionate about
            technology and continuous improvement. Here in <Em>TechShila Minds</Em>,
            we are not just trainers; we are your partners in growth and success.
          </Text>
          <Flex
            rounded="full"
            borderWidth="1px"
            flexDirection="row"
            alignItems="center"
            py="1"
            ps="8"
            pe="2"
            bg="primary.900"
            _dark={{ bg: "gray.900" }}
          >
            <Box>
              <Text color="yellow.400" display="inline">
               Learn
              </Text>{" "}
              <Text color="cyan.300" display="inline">
                More about us
              </Text>
            </Box>
            <IconButton
              icon={hasCopied ? <FiCheck /> : <FiCopy />}
              aria-label="Copy install command"
              onClick={onCopy}
              variant="ghost"
              ms="4"
              isRound
              color="white"
            />
          </Flex>
        </VStack>
      </HighlightsItem>
      <HighlightsItem title="Labs">
        <Text color="muted" fontSize="lg">
          We provide <Em>cutting-edge lab environments </Em>
          designed to enhance your learning experience and
          ensure the success of your IT training. Our fully
          equipped labs offer a hands-on, immersive experience,
          allowing you to apply theoretical knowledge in real-world scenarios.
          Whether your training in <Em>cybersecurity, data science,
          software development, or any other IT domain,</Em>
          our labs are tailored to meet the highest industry standards.
          Join us and transform your training with our exceptional lab facilities.
        </Text>
      </HighlightsItem>
      <HighlightsTestimonialItem
        name="Engineering Classes"
        description="Tutor"
        avatar="/static/images/avatar.jpg"
        gradient={["pink.200", "purple.500"]}
      >
        “Unlock the power of innovation! Dive into our engineering classes where creativity meets technical prowess. From cutting-edge projects to hands-on learning,
        fuel your passion and shape the future with us.
        Join now and engineer your success!”
      </HighlightsTestimonialItem>
      <HighlightsItem
        colSpan={[1, null, 2]}
        title="Shape and automate your next idea two steps ahead"
      >
        <Text color="muted" fontSize="lg">
          We took care of all your basic to advanced app development needs, so you can start
          building functionality that makes your product unique aand automate all your
          manual processes with our vast
          expertise
        </Text>
        <Wrap mt="8">
          {[
            "authentication",
            "navigation",
            "crud",
            "settings",
            "multi-tenancy",
            "layouts",
            "billing",
            "a11y testing",
            "server-side rendering",
            "documentation",
            "onboarding",
            "storybooks",
            "theming",
            "upselling",
            "unit testing",
            "feature flags",
            "responsiveness",
          ].map((value) => (
            <Tag
              key={value}
              variant="subtle"
              colorScheme="purple"
              rounded="full"
              px="3"
            >
              {value}
            </Tag>
          ))}
        </Wrap>
      </HighlightsItem>
    </Highlights>
  );
};

const FeaturesSection = () => {
  return (
    <Features
      id="features"
      title={
        <Heading
          lineHeight="short"
          fontSize={["2xl", null, "4xl"]}
          textAlign="left"
          as="p"
        >
          Not your standard
          <Br /> dashboard template.
        </Heading>
      }
      description={
        <>
          Saas UI Pro includes everything you need to build modern frontends.
          <Br />
          Use it as a template for your next product or foundation for your
          design system.
        </>
      }
      align="left"
      columns={[1, 2, 3]}
      iconSize={4}
      features={[
          // {
          //   title: "Components.",
          //   icon: FiBox,
          //   description:
          //     "All premium components are available on a private NPM registery, no more copy pasting and always up-to-date.",
          //   variant: "inline",
          // },
          // {
          //   title: "Starterkits.",
          //   icon: FiLock,
          //   description:
          //     "Example apps in Next.JS, Electron. Including authentication, billing, example pages, everything you need to get started FAST.",
          //   variant: "inline",
          // },
          // {
          //   title: "Documentation.",
          //   icon: FiSearch,
          //   description:
          //     "Extensively documented, including storybooks, best practices, use-cases and examples.",
          //   variant: "inline",
          // },
          // {
          //   title: "Onboarding.",
          //   icon: FiUserPlus,
          //   description:
          //     "Add user onboarding flows, like tours, hints and inline documentation without breaking a sweat.",
          //   variant: "inline",
          // },
          // {
          //   title: "Feature flags.",
          //   icon: FiFlag,
          //   description:
          //     "Implement feature toggles for your billing plans with easy to use hooks. Connect Flagsmith, or other remote config services once you're ready.",
          //   variant: "inline",
          // },
          // {
          //   title: "Upselling.",
          //   icon: FiTrendingUp,
          //   description:
          //     "Components and hooks for upgrade flows designed to make upgrading inside your app frictionless.",
          //   variant: "inline",
          // },
          // {
          //   title: "Themes.",
          //   icon: FiToggleLeft,
          //   description:
          //     "Includes multiple themes with darkmode support, always have the perfect starting point for your next project.",
          //   variant: "inline",
          // },
          // {
          //   title: "Generators.",
          //   icon: FiTerminal,
          //   description:
          //     "Extend your design system while maintaininig code quality and consistency with built-in generators.",
          //   variant: "inline",
          // },
          // {
          //   title: "Monorepo.",
          //   icon: FiCode,
          //   description: (
          //     <>
          //       All code is available as packages in a high-performance{" "}
          //       <Link href="https://turborepo.com">Turborepo</Link>, you have full
          //       control to modify and adjust it to your workflow.
          //     </>
          //   ),
          //   variant: "inline",
          // },
      ]}
    />
  );
};

const TestimonialsSection = () => {
  const columns = React.useMemo(() => {
    return testimonials.items.reduce<Array<typeof testimonials.items>>(
      (columns, t, i) => {
        columns[i % 3].push(t);

        return columns;
      },
      [[], [], []]
    );
  }, []);

  return (
    <Testimonials
      title={testimonials.title}
      columns={[1, 2, 3]}
      innerWidth="container.xl"
    >
      <>
        {columns.map((column, i) => (
          <Stack key={i} spacing="8">
            {column.map((t, i) => (
              <Testimonial key={i} {...t} />
            ))}
          </Stack>
        ))}
      </>
    </Testimonials>
  );
};

const PricingSection = () => {
  return (
    <Pricing {...pricing}>
      <Text p="8" textAlign="center" color="muted">
        VAT may be applicable depending on your location.
      </Text>
    </Pricing>
  );
};

const FaqSection = () => {
  return <AboutUs {...faq} />;
};

export default Home;

export async function getStaticProps() {
  return {
    props: {
      announcement: {
        title: "Support us by becoming a stargazer! 🚀 ",
        description:
          '<img src="https://img.shields.io/github/stars/saas-js/saas-ui.svg?style=social&label=Star" />',
        href: "https://github.com/saas-js/saas-ui",
        action: false,
      },
    },
  };
}
