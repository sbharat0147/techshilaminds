import {
  Box, Card, Grid,
  Heading,
  HStack,
  Icon,
  SimpleGrid, Stack,
  StackProps,
  Text,
  VStack,
} from "@chakra-ui/react";
import {
  ButtonLink,
  ButtonLinkProps,
} from "components/button-link/button-link";
import { BackgroundGradient } from "components/gradients/background-gradient";
import { Section, SectionProps, SectionTitle } from "components/section";
import React, {useState} from "react";
import { FiCheck } from "react-icons/fi";
import {GoHeading} from "react-icons/go";
import {SectionTitleTyped} from "components/section/section-title-typed";

export interface PricingPlan {
  id: string;
  title: React.ReactNode;
  description: React.ReactNode;
  price: React.ReactNode;
  features: Array<PricingFeatureProps | null>;
  action: ButtonLinkProps & { label?: string };
  isRecommended?: boolean;
}

export interface PricingProps extends SectionProps {
  description: React.ReactNode;
  plans: Array<PricingPlan>;
}

const items = [
  {
    videoId: 'dQw4w9WgXcQ', // Example YouTube video ID
    title: '',
    description:
        "",
  },
  // {
  //   videoId: 'eY52Zsg-KVI', // Example YouTube video ID
  //   title: 'Unlocking Academic Excellence',
  //   description:
  //       'Your key to seamless administration, insightful analytics, and empowering students for success.',
  // },
  // {
  //   videoId: 'IcrbM1l_BoI', // Example YouTube video ID
  //   title: 'Great user experience',
  //   description:
  //       'Integrate our product into your routine with an intuitive and easy-to-use interface.',
  // },
  // {
  //   videoId: 'mH0_XpSHkZo', // Example YouTube video ID
  //   title: 'Innovative functionality',
  //   description:
  //       'Stay ahead with features that set new standards, addressing your evolving needs better than the rest.',
  // },
  // {
  //   videoId: 'bTqVqk7FSmY', // Example YouTube video ID
  //   title: 'Reliable support',
  //   description:
  //       'Count on our responsive customer support, offering assistance that goes beyond the purchase.',
  // },
  // {
  //   videoId: 'Uj1ykZWtPYI', // Example YouTube video ID
  //   title: 'Precision in every detail',
  //   description:
  //       'Enjoy a meticulously crafted product where small touches make a significant impact on your overall experience.',
  // },
];


export const Pricing: React.FC<PricingProps> = (props) => {
  const { children, plans, title, description, ...rest } = props;
  const [hoveredIndex, setHoveredIndex] = useState(null);

  return (
    <Section id="pricing" pos="relative" {...rest}>
      <BackgroundGradient height="100%" />
      <Box zIndex="2" pos="relative">
        <SectionTitle title={title} description={description}></SectionTitle>

        <SimpleGrid columns={[1, null, 3]} spacing={4}>
          {plans?.map((plan) => (
            <PricingBox
              key={plan.id}
              title={plan.title}
              description={plan.description}
              price={plan.price}
              sx={
                plan.isRecommended
                  ? {
                      borderColor: "primary.500",
                      _dark: {
                        borderColor: "primary.500",
                        bg: "blackAlpha.300",
                      },
                    }
                  : {}
              }
            >
              {/*<PricingFeatures>*/}
              {/*  {plan.features.map((feature, i) =>*/}
              {/*    feature ? (*/}
              {/*      <PricingFeature key={i} {...feature} />*/}
              {/*    ) : (*/}
              {/*      <br key={i} />*/}
              {/*    )*/}
              {/*  )}*/}
              {/*</PricingFeatures>*/}
              {/*<ButtonLink colorScheme="primary" {...plan.action}>*/}
              {/*  {plan.action.label || "Sign Up"}*/}
              {/*</ButtonLink>*/}
              <Grid>
                {items.map((item, index) => (
                    <Grid sm={6} md={4} key={index}>
                      <Stack
                          direction="column"
                          color="inherit"
                          // component={Card}
                          spacing={1}
                          useFlexGap
                          sx={{
                            p: 3,
                            height: '100%',
                            border: '1px solid',
                            borderColor: 'grey.800',
                            background: 'transparent',
                            backgroundColor: 'grey.900',
                            '&:hover': {
                              cursor: 'pointer',
                            },
                          }}
                          onMouseEnter={() => setHoveredIndex(index)}
                          onMouseLeave={() => setHoveredIndex(null)}
                      >
                        <Box
                            sx={{
                              position: 'relative',
                              height: 0,
                              paddingBottom: '56.25%', // 16:9 aspect ratio
                              width: '100%',
                              overflow: 'hidden',
                              backgroundColor: 'black',
                            }}
                        >
                          {hoveredIndex === index ? (
                              <iframe
                                  width="100%"
                                  height="100%"
                                  src={`https://www.youtube.com/embed/${item.videoId}?autoplay=1&mute=1`}
                                  frameBorder="0"
                                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                  allowFullScreen
                                  style={{
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    width: '100%',
                                    height: '100%',
                                  }}
                              />
                          ) : (
                              <img
                                  src={`https://img.youtube.com/vi/${item.videoId}/hqdefault.jpg`}
                                  alt={item.title}
                                  style={{
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    width: '100%',
                                    height: '100%',
                                    objectFit: 'cover',
                                  }}
                              />
                          )}
                        </Box>
                        <div>
                          <Heading fontWeight="medium">
                            {item.title}
                          </Heading>
                          <GoHeading sx={{ color: 'grey.400' }}>
                            {item.description}
                          </GoHeading>
                        </div>
                      </Stack>
                    </Grid>
                ))}
              </Grid>
            </PricingBox>
          ))}
        </SimpleGrid>

        {children}
      </Box>
    </Section>
  );
};

const PricingFeatures: React.FC<React.PropsWithChildren<{}>> = ({
  children,
}) => {
  return (
    <VStack
      align="stretch"
      justifyContent="stretch"
      spacing="4"
      mb="8"
      flex="1"
    >
      {children}
    </VStack>
  );
};

export interface PricingFeatureProps {
  title: React.ReactNode;
  iconColor?: string;
}

const PricingFeature: React.FC<PricingFeatureProps> = (props) => {
  const { title, iconColor = "primary.500" } = props;
  return (
    <HStack>
      <Icon as={FiCheck} color={iconColor} />
      <Text flex="1" fontSize="sm">
        {title}
      </Text>
    </HStack>
  );
};

export interface PricingBoxProps extends Omit<StackProps, "title"> {
  title: React.ReactNode;
  description: React.ReactNode;
  price: React.ReactNode;
}

const PricingBox: React.FC<PricingBoxProps> = (props) => {
  const { title, description, price, children, ...rest } = props;
  return (
    <VStack
      zIndex="2"
      bg="whiteAlpha.600"
      borderRadius="md"
      p="8"
      flex="1 0"
      alignItems="stretch"
      border="1px solid"
      borderColor="gray.400"
      _dark={{
        bg: "blackAlpha.300",
        borderColor: "gray.800",
      }}
      {...rest}
    >
      <Heading as="h3" size="md" fontWeight="bold" fontSize="lg" mb="2">
        {title}
      </Heading>
      <Box color="muted">{description}</Box>
      <Box fontSize="2xl" fontWeight="bold" py="4">
        {price}
      </Box>
      <VStack align="stretch" justifyContent="stretch" spacing="4" flex="1">
        {children}
      </VStack>
    </VStack>
  );
};
