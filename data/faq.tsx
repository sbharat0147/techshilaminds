import * as React from 'react'
import {Em} from "components/typography";

const faq = {
  title: 'About Us',
  // description: '',
  items: [
    {
      q: 'How many products can I use Saas UI Pro for?',
      a: (
        <>
                Indulge in the pinnacle of technological sophistication with <Em>TechShila Minds</Em>,
          where each interaction is a harmonious symphony of expertise, innovation, and elegance.
          With a legacy steeped in the artistry of IT training and bespoke software craftsmanship,
          our establishment, born of passion and nurtured by experience, epitomizes the epitome of
          excellence. Embark on a journey of enlightenment, where cutting-edge labs and visionary
          minds converge to sculpt futures brimming with possibility. At <Em>TechShila Minds</Em>,
          we transcend convention, weaving intricate tapestries of code and creativity, tailored
          to the bespoke aspirations of our discerning clientele. Elevate your aspirations, embrace
          the allure of innovation, and embark on a voyage of transformation with us, where every
          endeavor is a testament to the exquisite artistry of technology.
        </>
      ),
    },
  ],
}

export default faq
